Thông tin chi tiết máy pha cafe breville 920:

Tham khảo: https://hiyams.com/may-pha-che-cafe/may-pha-cafe/may-pha-cafe-breville/may-pha-cafe-breville-920/

Máy pha cafe Breville 920 hay còn được gọi là Breville The Dual Boiler kiểu dáng chắc chắn. Với kích thước chiều dài 37cm, chiều rộng 37cm và chiều cao 40cm, không mất quá nhiều diện tích đặt máy.
Máy pha cafe Breville 920 với công suất lên đến 2.200W rất thích hợp với quầy, quán, khách sạn, công ty… Thuận lợi để pha chế những ly Espresso, Cappuccino, Latte, Americano… đúng chuẩn như một Barista thực thụ.
Máy pha cafe Breville 920 chỉ sử dụng cafe bột để pha chế. Bạn có thể kết hợp với máy xay cafe Breville Smart Grinder Pro để thuận tiện hơn cho quá trình pha chế.

Thông số kỹ thuật Máy pha cafe Breville 920:
Xuất xứ: Australia
Vật liệu: Thép không gỉ
Điện áp: 220 – 240 Volts
Công suất: 1700W (thermocoli)
Tần suất sử dụng phù hợp: 100 – 200 ly/ngày
Áp suất nồi hơi: 15 bar Italian Pump
Kích thước: 40.5 cm x 37.3 cm x 37.7 cm
Trọng lượng: 16.5 kg
Bảo hành: 1 năm